;; OO system #1: Prototype (classless) objects with single inheritance.
;;

;; Copyright 2010 Rob Tillotson <rob@pyrite.org>
;;
;; Permission is hereby granted, free of charge, to any person obtaining a copy
;; of this software and associated documentation files (the "Software"), to deal
;; in the Software without restriction, including without limitation the rights
;; to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
;; copies of the Software, and to permit persons to whom the Software is
;; furnished to do so, subject to the following conditions:
;;
;; The above copyright notice and this permission notice shall be included in
;; all copies or substantial portions of the Software.
;;
;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
;; IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
;; AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
;; OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
;; THE SOFTWARE.



;; An object is a closure with variables for parent and self, and a
;; hash table of slots.  A method is just a slot with a procedure in
;; it, which will be called with the object as its first parameter.
;; A few built in methods are added to the slot table to handle
;; slot setting and deletion, object cloning etc.
;;
;; To call or retrieve a slot, simply call the object with the method
;; name as the first parameter, eg.  (object 'x 1 2)
;;
;; Builtin method protocol:
;;
;;   parent             - (core) return the parent object
;;   self               - (core) return the object itself
;;   responds-to? sym   - (core) #t if responds to a selector
;;   slot-set! sym val  - (core) set the value of a slot
;;   slot-delete! sym   - (core) delete a slot locally
;;                        (exposing the parent slot, if any)
;;   clone              - (core) create new object with same parent
;;                        and copy of slots
;;   ::unknown-selector - if present, this slot is called with (self selector args ...)
;;                        any time a selector lookup fails
;;   ::slots            - (core) return alist of local slots
;;   ::name             - symbol or string for an object name to be printed
;;                        in error messages and the like
;;   parents            - return a list of all parents of the object
;;   ->alist            - like __slots__ but includes inherited slots
;;   ->string           - convert object to string
;;
;; Some things this implementation lacks which might be nice to have
;; later on... or not:
;;
;;   - an object wrapper for basic types
;;   - a comparison protocol
;;   - any notion of an object identity (but you can probably use
;;     eq? to see if they're the same one or have the same parent
;;     etc.)
;;   - serialization
;;

(module cryptic-oo *
        (import scheme chicken)
        (use srfi-1 srfi-13 srfi-69 ports)

        (define (create-object parent-object . slot-defs)
          (let ((parent #f)
                (self #f)
                (slots (make-hash-table eq? symbol-hash)))
            
            ;; Builtin methods
            (define (responds-to? self method)
              (cond ((hash-table-exists? slots method) #t)
                    (parent (parent 'responds-to? method))
                    (else #f)))
            (define (clone self)
              (apply create-object parent slots))
            
            ;; Helper function to set a slot's value.
            (define (slot-set! self slotname value)
              (hash-table-set! slots slotname value))

            (define (slot-delete! self slotname)
              (hash-table-delete! self slotname))

            (define (add-slots alist)
              (map (lambda (pair) (slot-set! self (car pair) (cadr pair))) alist))

            ;; Retrieve a slot, going up the inheritance tree if needed.
            (define (slot-get self selector . args)
              (cond ((hash-table-exists? slots selector) (hash-table-ref slots selector))
                    (parent (apply parent 'slot-get selector args))
                    (else (if (null? args)
                              (raise-unknown-selector-exn selector)
                              (car args)))))
            
            ;; Handle unknown selectors during dispatch.
            (define (handle-unknown-selector self selector . args)
              (if (responds-to? self '::unknown-selector)
                  (apply self '::unknown-selector selector args)
                  (raise-unknown-selector-exn selector)))

            ;; Raise an unknown selector exception with a useful message.
            (define (raise-unknown-selector-exn selector)
              (abort
               (make-composite-condition unknown-selector-exn
                                         (make-property-condition 'exn
                                                                  'message (with-output-to-string
                                                                             (lambda ()
                                                                               (display "Unknown selector ")
                                                                               (display selector)
                                                                               (display " in object ")
                                                                               (display (self '::name))))))))
            
            ;; Dispatcher.  Retrieve the slot and call it if necessary.  If the slot is not
            ;; found, we return one that calls handle-unknown-selector instead.
            ;;
            ;; The selector can be a procedure, in which case it is called with the object as
            ;; its first parameter.  Otherwise, it is looked up as normal.
            (define (dispatch selector . args)
              (if (procedure? selector)
                  (apply selector self args)
                  
                  (let ((slot (slot-get self selector
                                        (lambda (self . args) (apply handle-unknown-selector self selector args)))))
                    (if (procedure? slot) (apply slot self args)
                        slot))))
            
            (add-slots `((parent        ,(lambda (self) parent))
                         (self          ,(lambda (self) self))
                         (responds-to?  ,responds-to?)
                         (clone         ,clone)
                         (slot-set!     ,slot-set!)
                         (slot-delete!  ,slot-delete!)
                         (slot-get      ,slot-get)
                         (::slots       ,(lambda (self) (hash-table->alist slots)))))
            (add-slots slot-defs)
            (set! parent parent-object)
            (set! self dispatch)
            self))

        ;; Exceptions that can be raised
        (define unknown-selector-exn
          (make-property-condition 'exn))

        ;; Syntactic sugar:
        ;;

        ;; (slot: name value)
        ;;   Define a slot at initialization time (returns ('name value)).
        (define-syntax slot:
          (syntax-rules ()
            ((_ slot value)
             (list (quote slot) value))))
        
        ;; (slot! object name value)
        ;; (slot-set! object name value)
        ;; (! object name value)
        ;;   Set a slot value by calling the object's set-slot method.
        (define-syntax slot!
          (syntax-rules ()
            ((_ object slot value)
             (object 'slot-set! (quote slot) value))))
        
        (define-syntax slot-set!
          (syntax-rules ()
            ((_ object slot value)
             (object 'slot-set! (quote slot) value))))
        
        (define-syntax !
          (syntax-rules ()
            ((_ object slot value)
             (object 'slot-set! (quote slot) value))))
        
        ;; (slot-delete! object name)
        ;;   Remove a slot from the object, if it exists, by calling
        ;;   the object's slot-delete! method.
        (define-syntax slot-delete!
          (syntax-rules ()
            ((_ object slot)
             (object 'slot-delete! (quote slot) value))))
        
        ;; (@ object slot . args)
        ;;   Call or return a slot value.
        ;;   Equivalent to (object 'slot arg ...)
        (define-syntax @
          (syntax-rules ()
            ((_ object slot arg ...)
             (object (quote slot) arg ...))))
        
        ;; (define-object name << parent slot ...)
        ;; (define-object name slot ...)
        ;;   Define and create a new object.  If no parent is given the parent
        ;;   will be <object>.  The ::name slot of the object will be set to the
        ;;   defined name.
        (define-syntax define-object
          (syntax-rules (<<)
            ((_ name << parent slot ...)
             (define name (create-object parent '(::name name) slot ...)))
            ((_ name slot ...)
             (define name (create-object <object> '(::name name) slot ...)))
            ))
        
        
        ;; And now, a base object to collect useful methods which
        ;; don't strictly have to be inside the object closure.
        ;;
        
        (define-object <object> << #f
          (slot: parents (lambda (self)
                           (let ((parent (self 'parent)))
                             (if parent
                                 (cons parent (parent 'parents))
                                 '()))))
          
          (slot: ->alist (lambda (self)
                           (let ((slots (self '::slots)))
                             (remove (lambda (x) (string-prefix? "::" (symbol->string (car x))))
                                     (append slots (remove (lambda (x) (assoc (car x) slots))
                                                           (if (self 'parent) ((self 'parent) '->alist) '())))))))
          (slot: ->string (lambda (self)
                            (with-output-to-string
                              (lambda ()
                                (display "{object: ")
                                (display (self 'slot-get '::name "(unknown)"))
                                (when (self 'parent)
                                  (display " << ")
                                  (display ((self 'parent) 'slot-get '::name "(unknown)")))
                                (display "}")))))
          )
        
        ;; (object? x) - test whether something is an object
        (define (object? obj)
          (handle-exceptions exn #f (obj 'responds-to? 'self)))
        
        ;; (object->string x)  - call __str__ on the object
        (define (object->string obj)
          (obj '->string))
        
        (define (object->alist obj)
          (obj '->alist))
        
        ;; (selector sel . args)
        ;;   Create a selector function which will call the selector on an object
        ;;   passed to it.
        ;;
        ;; (:: sel . args)
        ;;   Same as 'selector', but with automatic quoting of the selector name.
        ;;
        ;; Because the dispatcher accepts procedures as selectors (in addition to
        ;; symbols) you can use this to make a curried selector suitable for
        ;; calling an object directly.
        ;;
        (define (selector sel . extra)
          (lambda (obj . args) (apply obj sel (append extra args))))
        
        (define-syntax ::
          (syntax-rules ()
            ((_ sel arg ...)
             (selector (quote sel) arg ...))))
        
        (define-syntax map-selector
          (syntax-rules ()
            ((_ sel objs arg ...)
             (map (selector (quote sel) arg ...) objs))))
        
        (define-syntax filter-selector
          (syntax-rules ()
            ((_ sel objs arg ...)
             (map (selector (quote sel) arg ...) objs))))
        
        (define-syntax fold-selector
          (syntax-rules ()
            ((_ sel knil objs ...)
             (fold (selector (quote sel)) knil objs ...))))
        
        )
